[![pipeline status](https://gitlab.com/grunlab/gost/badges/main/pipeline.svg)](https://gitlab.com/grunlab/gost/-/commits/main)

# GrunLab Gost

Gost non-root container image.

Docs:
- https://docs.grunlab.net/images/gost.md
- https://docs.grunlab.net/apps/gost.md

Base image: [grunlab/base-image/debian:12][base-image]

Format: docker

Supported architecture(s):
- arm-v7

GrunLab project(s) using this service:
- [grunlab/aria2][aria2]

[base-image]: <https://gitlab.com/grunlab/base-image>
[aria2]: <https://gitlab.com/grunlab/aria2>

